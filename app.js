import express from "express";
import ip from "ip";
import cors from "cors";
import bodyParser from "body-parser";
import morgan from "morgan";
import Axios from "axios";

const app = express();
app.use(cors());
app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

app.get("/", (req, res) => {
  res.json({ message: "Hello from the server!", server: ip.address() });
});

app.post("/predict", (req, res) => {
  const { text } = req.body;
  let total = 0;
  let x = text.split(".");
  const ipaddr = "10.1.1." + getRandomInt(45, 50);
  const obj = { text: x };
  Axios.post("http://localhost:5000/model/predict", obj).then(response => {
    const { data } = response;
    const { predictions } = data;
    predictions.forEach(element => {
      total += element["positive"];
    });
    let average = total / predictions.length;
    res.json({
      message: "Successful",
      reducerIP: ipaddr,
      polarity: average,
      result: data
    });
  });
});

const port = process.env.PORT || 9000;
app.listen(port, () => {
  console.log("Running on:", port);
});
